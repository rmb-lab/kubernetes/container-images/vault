FROM vault:0.10.4

ENV CONSUL_TEMPLATE_VERSION=0.19.5
RUN apk -U add jq curl openssl && \
    curl https://releases.hashicorp.com/consul-template/${CONSUL_TEMPLATE_VERSION}/consul-template_${CONSUL_TEMPLATE_VERSION}_linux_amd64.zip -o consul-template.zip && \
    unzip consul-template.zip -d /bin && \
    rm -rf consul-template.zip && \
    mkdir -p /consul-template/config && \
    mkdir -p /consul-template/templates

COPY consul-template/config/config.hcl /consul-template/config/config.hcl
COPY consul-template/templates /consul-template/templates
RUN chown -R vault:vault /consul-template

COPY vault-init.sh /usr/local/bin/vault-init.sh
COPY consul-setup.sh /usr/local/bin/consul-setup.sh
COPY kubernetes-setup.sh /usr/local/bin/kubernetes-setup.sh
COPY unseal.sh /usr/local/bin/unseal.sh
COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh