#!/bin/sh
set -e

if [ $# -eq 0 ]; then
  echo "No pki not supplied"
  exit 1
fi

echo "Enter Vault Unseal Key: "
read VAULT_UNSEAL_KEY

echo "Generating Root Token"
export VAULT_SKIP_VERIFY=true
export OTP=`vault operator generate-root -generate-otp`
export NONCE=`vault operator generate-root -init -otp=${OTP} -format=json | jq -r .nonce`
export ENCODED_ROOT=`vault operator generate-root -nonce=${NONCE} -format=json ${VAULT_UNSEAL_KEY} | jq -r '.encoded_root_token'`
export VAULT_TOKEN=`vault operator generate-root -decode=${ENCODED_ROOT} -otp=${OTP}`

echo "Enter CA Certificate (ctrl-d when done): "
export ca_cert=$(cat)

echo "Enter CA Key (ctrl-d when done): "
export ca_key=$(cat)

vault write pki_$1/config/ca \
  pem_bundle="${ca_key}\n${ca_cert}"
rm -rf ~/.vault-token