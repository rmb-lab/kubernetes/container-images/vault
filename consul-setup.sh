#!/bin/sh
set -e

echo "Enter Vault Unseal Key: "
read VAULT_UNSEAL_KEY

echo "Generating Root Token"
export VAULT_SKIP_VERIFY=true
export OTP=`vault operator generate-root -generate-otp`
export NONCE=`vault operator generate-root -init -otp=${OTP} -format=json | jq -r .nonce`
export ENCODED_ROOT=`vault operator generate-root -nonce=${NONCE} -format=json ${VAULT_UNSEAL_KEY} | jq -r '.encoded_root_token'`
export VAULT_TOKEN=`vault operator generate-root -decode=${ENCODED_ROOT} -otp=${OTP}`
echo "Finding consul acl token"
export CONSUL_TOKEN=`vault read -format=json secret/consul-server/acl-token | jq -r .data.value`
echo "Configuring vault consul access"
vault write consul/config/access \
  address=consul.kube0-ing.vmw.rmb938.me \
  scheme=https \
  token=${CONSUL_TOKEN}
rm -rf ~/.vault-token