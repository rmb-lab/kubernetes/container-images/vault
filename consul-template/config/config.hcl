vault {
  address     = "https://vault.rmb938.me:8200/"
  renew_token = true

  # Disable ssl verification
  # We are on the Vault server so we have no "secure" way to grab the root CA from Vault
  # So verifying stuff is pointless
  ssl {
    verify = false
  }
}

exec {
  kill_signal = "SIGHUP"
}

template {
  source      = "/consul-template/templates/vault.crt.ctmpl"
  destination = "/vault/tls/tls.crt"
}

template {
  source      = "/consul-template/templates/vault.key.ctmpl"
  destination = "/vault/tls/tls.key"
}
