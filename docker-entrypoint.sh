#!/usr/bin/dumb-init /bin/sh
set -e

# Allow setting VAULT_REDIRECT_ADDR and VAULT_CLUSTER_ADDR using an interface
# name instead of an IP address. The interface name is specified using
# VAULT_REDIRECT_INTERFACE and VAULT_CLUSTER_INTERFACE environment variables. If
# VAULT_*_ADDR is also set, the resulting URI will combine the protocol and port
# number with the IP of the named interface.
get_addr () {
    local if_name=$1
    local uri_template=$2
    ip addr show dev $if_name | awk -v uri=$uri_template '/\s*inet\s/ { \
      ip=gensub(/(.+)\/.+/, "\\1", "g", $2); \
      print gensub(/^(.+:\/\/).+(:.+)$/, "\\1" ip "\\2", "g", uri); \
      exit}'
}

if [ -n "$VAULT_REDIRECT_INTERFACE" ]; then
    export VAULT_REDIRECT_ADDR=$(get_addr $VAULT_REDIRECT_INTERFACE ${VAULT_REDIRECT_ADDR:-"http://0.0.0.0:8200"})
    echo "Using $VAULT_REDIRECT_INTERFACE for VAULT_REDIRECT_ADDR: $VAULT_REDIRECT_ADDR"
fi
if [ -n "$VAULT_CLUSTER_INTERFACE" ]; then
    export VAULT_CLUSTER_ADDR=$(get_addr $VAULT_CLUSTER_INTERFACE ${VAULT_CLUSTER_ADDR:-"https://0.0.0.0:8201"})
    echo "Using $VAULT_CLUSTER_INTERFACE for VAULT_CLUSTER_ADDR: $VAULT_CLUSTER_ADDR"
fi

mkdir -p /vault/tls/
chown -R vault:vault /vault/tls 
chown -R vault:vault /vault/config || echo "Could not chown /vault/config (may not have appropriate permissions)"
setcap cap_ipc_lock=+ep $(readlink -f $(which vault))

if [ -z "$BOOTSTRAP" ]
then
    export VAULT_SKIP_VERIFY=true
    export VAULT_ADDR=https://vault.rmb938.me:8200/
    echo "Trying to auth to existing vault servers to get certs"
    while [ -z "$VAULT_TOKEN" ]
    do
        export VAULT_TOKEN=$(vault write -field token auth/kubernetes/login role=vault-server jwt=`cat /var/run/secrets/kubernetes.io/serviceaccount/token`)
    done
    echo "Auth successful"
    su-exec vault:vault consul-template -config "/consul-template/config" -exec "vault server -config='/vault/config'"
else
    echo "In bootstrap mode, using self-issued certificates"
    openssl req -new -newkey rsa:4096 -x509 -sha256 -days 365 -nodes -out /vault/tls/tls.crt -keyout /vault/tls/tls.key -subj "/C=US/ST=Minnesota/L=Minneapolis/O='Home Lab'/CN=vault"
    chown -R vault:vault /vault/tls
    su-exec vault:vault vault server -config="/vault/config"
fi
