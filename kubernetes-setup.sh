#!/bin/sh

echo "Enter Vault Unseal Key: "
read VAULT_UNSEAL_KEY

echo "Generating Root Token"
export VAULT_SKIP_VERIFY=true
export OTP=`vault operator generate-root -generate-otp`
export NONCE=`vault operator generate-root -init -otp=${OTP} -format=json | jq -r .nonce`
export ENCODED_ROOT=`vault operator generate-root -nonce=${NONCE} -format=json ${VAULT_UNSEAL_KEY} | jq -r '.encoded_root_token'`
export VAULT_TOKEN=`vault operator generate-root -decode=${ENCODED_ROOT} -otp=${OTP}`
echo "Configuring vault kubernetes access"
vault write auth/kubernetes/config \
    token_reviewer_jwt=@/var/run/secrets/kubernetes.io/serviceaccount/token \
    kubernetes_host=https://kubernetes.default \
    kubernetes_ca_cert=@/var/run/secrets/kubernetes.io/serviceaccount/ca.crt
rm -rf ~/.vault-token
