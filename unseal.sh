#!/usr/bin/dumb-init /bin/sh

export VAULT_SKIP_VERIFY=true

while true; do
  if [ -z "$VAULT_UNSEAL_KEY" ]
  then
    echo "No unseal token set, cannot unseal!"
  else
    vault status
    if [ $? -eq 2 ]
    then
      vault operator unseal $VAULT_UNSEAL_KEY
    fi
  fi
  sleep 30
done