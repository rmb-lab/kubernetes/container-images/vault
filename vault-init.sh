#!/bin/sh

export VAULT_SKIP_VERIFY=true
vault operator init -key-shares=1 -key-threshold=1
